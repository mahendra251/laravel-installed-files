<!DOCTYPE html>
<html>
<head>
	<title>MotoBlock Chain</title>
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.css') }}">
	<script type="text/javascript" src=" {{ url('assets/js/jquery-3.4.0.min.js') }} "></script>
	<script type="text/javascript" src=" {{ url('assets/js/bootstrap.js') }} "></script>
</head>
<body>
		@include('inc/header')
			<div class="container">
				@if (count($errors) > 0)
		   <div class = "alert alert-danger">
		      <ul>
		         @foreach ($errors->all() as $error)
		            <li>{{ $error }}</li>
		         @endforeach
		      </ul>
		   </div>
		@endif
		@if(session('info'))
					<div class="alert alert-danger col-sm-12">
						{{ session('info') }}
					</div>
			@endif
	
	
			<form method="post" action="{{ url('/step_1_reg_insert') }}" enctype="multipart/form-data">
				{{ csrf_field() }}
			  <fieldset>
			    <legend>Owners Bike registration</legend>
			    <input type="hidden" name="user_id" value="{{ session()->get('auth_user_id') }}">
			    <div class="form-group">
			      <label for="exampleSelect1">no. of motor cycles</label>
			      <select class="form-control" name="no_of_motor_cycle" id="exampleSelect1">
			        <option value="1"> 1 </option>
			        <option value="2"> 2 </option>
			        <option value="3"> 3 </option>
			        <option value="4"> 4 </option>
			        <option value="5"> 5 </option>
			      </select>
			      {!! $errors->first('no_of_motor_cycle', '<span class="help-block text-danger"> :message </span>') !!}
			    </div>
			    
			   <div class="row">
				    <div class="form-group col-sm-6">
				      <label for="exampleInputEmail1">Brand Name </label>
				      <select  class="form-control" name="brand_id" id="brand_id">
					   <option value="">Marca</option>
					   @foreach($brands as $key => $brand)
		                  <option value="{{$key}}"> {{$brand}}</option>
		                @endforeach
					</select>
				      {!! $errors->first('brand_id', '<span class="help-block text-danger"> :message </span>') !!}
				      
				    </div>
				    <div class="form-group col-sm-6">
				      <label for="exampleInputPassword1">Model</label>
				      <select  name="model_id" id="model_id" class="form-control">
						  
						</select>
						 {!! $errors->first('model_id', '<span class="help-block text-danger"> :message </span>') !!}
				    </div>
				   </div>
				<div class="row">
					<div class="form-group col-sm-6">
				      <label for="exampleInputEmail1">frame no</label>
				      <input type="text" name="frame_no" class="form-control" id="exampleInputEmail1" placeholder="Enter frame no">
				      {!! $errors->first('frame_no', '<span class="help-block text-danger"> :message </span>') !!}
				      
				    </div>
				    <div class="form-group col-sm-6">
				      <label for="exampleInputEmail1">upload a picture of the original motorbike’s documentation</label>
				      <input type="file" name="frame_img" class="form-control-file" id="exampleInputEmail1">
				      {!! $errors->first('frame_img', '<span class="help-block text-danger"> :message </span>') !!}
				      
				    </div>

				</div>
			    <div class="form-group">
			      <label for="exampleInputEmail1">Bike purchase date</label>
			      <input type="date" name="purchase_date" class="form-control" id="exampleInputEmail1" placeholder="Enter DNI">
			      {!! $errors->first('purchase_date', '<span class="help-block text-danger"> :message </span>') !!}
			      
			    </div>
			    <div class="form-group">
			      <input type="radio" name="new_or_used" class="new_or_used" value="new"> Bought the motorbike New &nbsp; &nbsp; 
			       <input type="radio" name="new_or_used" class="new_or_used" value="used"> bought the motorbike Used
			       <br>
			      {!! $errors->first('new_or_used', '<span class="help-block text-danger"> :message </span>') !!}
			      
			    </div>
			    <div class="form-group" id="previous_owner_no" style="display: none;">
			      <label for="exampleInputEmail1">Put the number of previous owners</label>
			      <input type="text" name="previous_owner_no" class="form-control" id="exampleInputEmail1" placeholder="Enter previous Owner name">
			      
			    </div>
			    <!--
			    <div class="form-group">
			    	<label for="bike_img">Bike Images</label>
			    	<input required type="file"  id="bike_img" class="form-control" name="images[]"  multiple>
			    </div> -->
			   <div class="form-group row">
			   	<label>Add Images of bike</label>
			     <div class="input-group control-group increment " >
		          <input type="file" name="filename[]" class="form-control">
		          <div class="input-group-btn"> 
		            <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
		          </div>
		        </div>
		        <div class="clone hide" style="display:none">
		          <div class="control-group input-group" style="margin-top:10px">
		            <input type="file" name="filename[]" class="form-control">
		            <div class="input-group-btn"> 
		              <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
		            </div>
		          </div>
		        </div>
		        {!! $errors->first('filename', '<span class="help-block text-danger"> :message </span>') !!}

		    </div>

			    <div class="row">
			    	<div class="form-group col-sm-6">
				      <label for="exampleInputEmail1">Mileage (MPH & Km/h)</label>
				      <input type="text" name="mileage" class="form-control" id="exampleInputEmail1" placeholder="MPH & Km/h">
				      {!! $errors->first('mileage', '<span class="help-block text-danger"> :message </span>') !!}
				      
				    </div>
				    <div class="form-group col-sm-6">
				      <label for="exampleInputEmail1">Mileage picture</label>
				      <input type="file" name="mileage_img" class="form-control" id="exampleInputEmail1" >
				      {!! $errors->first('mileage_img', '<span class="help-block text-danger"> :message </span>') !!}
				      
				    </div>
			    </div>
			    

			    
			    
			    <button type="submit" class="btn btn-primary">Submit</button>
			  </fieldset>
			</form>
	</div>

<script type="text/javascript">


    $(document).ready(function() {

      $(".btn-success").click(function(){ 
      	$(".clone").show();
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });

      $(".new_or_used").click(function(){
	      if($(this).val() === "used")
	        $("#previous_owner_no").show("fast");
	      else
	        $("#previous_owner_no").hide("fast");
	    });

    });

</script>
<script type="text/javascript">
    $('#brand_id').change(function(){
    var brand_id = $(this).val();    
    if(brand_id){
        $.ajax({
           type:"GET",
           url:"{{url('get-model-list')}}?brand_id="+brand_id,
           success:function(res){               
            if(res){
                $("#model_id").empty();
                $("#model_id").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#model_id").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#model_id").empty();
            }
           }
        });
    }else{
        $("#model_id").empty();
        
    }      
   });
   
</script>

</body>
</html>