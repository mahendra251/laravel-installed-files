
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <a class="navbar-brand" href="{{ url('/') }}">Navbar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Features</a>
          </li>
          
        </ul>
        
        <form class="form-inline my-2 my-lg-0">
          <a class="btn btn-success my-2 my-sm-0" href="{{ url('/register') }}" >Register</a>  &nbsp; 
          <a class="btn btn-success my-2 my-sm-0" href="{{ url('/login') }}" > Login</a> &nbsp;  

          <a class="btn btn-success my-2 my-sm-0" href="{{ url('/logout') }}" > Logout</a> &nbsp;  
        </form>
      </div>
    </nav>

