<!DOCTYPE html>
<html>
<head>
	<title>MotoBlock Chain</title>
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.css') }}">
	<script type="text/javascript" src=" {{ url('assets/js/jquery-3.4.0.min.js') }} "></script>
	<script type="text/javascript" src=" {{ url('assets/js/bootstrap.js') }} "></script>
</head>
<body>
	@include('inc/header')
	<div class="container">
	<div class="row">
		<div class="col-sm-3">
		</div>
		<div class="col-sm-6">
			@if(session('info'))
					<div class="alert alert-danger col-sm-12">
						{{ session('info') }}
					</div>
				@endif

			
			
			<form action="{{ url('/forgot_pass') }}" method="post">
			{{ csrf_field() }}
			  <fieldset>
			    <legend>Forgot nyht Password</legend>
			    <div class="form-group ">
			    	
			      <label for="staticEmail" class=" col-form-label">Email</label>
			      
			        <input type="text" name="email" class="form-control" id="staticEmail" Placeholder="Enter Email">
			        {!! $errors->first('email', '<span class="help-block text-danger"> :message </span>') !!}
			   
			    </div>
			    
			    <button type="submit" class="btn btn-primary">Submit</button>
			  </fieldset>
			</form>

		</div>
		<div class="col-sm-3">
		</div>

	</div>
		
	</div>

</body>
</html>