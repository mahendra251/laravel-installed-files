<!DOCTYPE html>
<html>
<head>
	<title>MotoBlock Chain</title>
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.css') }}">
	<script type="text/javascript" src=" {{ url('assets/js/jquery-3.4.0.min.js') }} "></script>
	<script type="text/javascript" src=" {{ url('assets/js/bootstrap.js') }} "></script>
</head>
<body>
	@include('inc/header')
	<div class="container">

	
			<form method="post" action="{{ url('/reg_step_1_insert') }}" enctype="multipart/form-data">
				{{ csrf_field() }}
			  <fieldset>
			    <legend>Legend</legend>
			    <input type="hidden" name="user_id" value="{{ session()->get('user_id') }}">
			    <div class="form-group">
			      <label for="exampleInputEmail1">Name</label>
			      <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter name">
			      {!! $errors->first('name', '<span class="help-block text-danger"> :message </span>') !!}
			      
			    </div>
			   <div class="row">
				    <div class="form-group col-sm-6">
				      <label for="exampleInputEmail1">first Surname </label>
				      <input type="text" name="surname1" class="form-control" id="exampleInputEmail1" placeholder="Enter First Sirname">
				      {!! $errors->first('surname1', '<span class="help-block text-danger"> :message </span>') !!}
				      
				    </div>
				    <div class="form-group col-sm-6">
				      <label for="exampleInputPassword1">second surname</label>
				      <input type="text" name="surname2" class="form-control" id="exampleInputPassword1" placeholder="second Sirname">
				    </div>
				   </div>
			    <div class="form-group">
			      <label for="exampleInputEmail1">DNI</label>
			      <input type="text" name="dni" class="form-control" id="exampleInputEmail1" placeholder="Enter DNI">
			      {!! $errors->first('dni', '<span class="help-block text-danger"> :message </span>') !!}
			      
			    </div>
			    <div class="form-group">
			      <label for="exampleInputEmail1">Nick Name</label>
			      <input type="text" name="nickname" class="form-control" id="exampleInputEmail1" placeholder="Enter NickName">
			      
			    </div>
			    <div class="row">
			    	<div class="form-group col-sm-4">
				      <label for="exampleInputEmail1">Country</label>
				      <input type="text" name="country" class="form-control"  placeholder="Enter Country">
				    </div>
				    <div class="form-group col-sm-4">
				      <label for="exampleInputEmail1">Region</label>
				      <input type="text" name="region" class="form-control"  placeholder="Enter region">
				    </div>
				    <div class="form-group col-sm-4">
				      <label for="exampleInputEmail1">city</label>
				      <input type="text" name="city" class="form-control"  placeholder="Enter City">
				    </div>
			    </div>
			    <div class="form-group">
			      <label for="exampleInputFile">Profile photo</label>
			      <input type="file" name="profile_pic" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
			      <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
			    </div>

			    <div class="form-group">
			      <label for="exampleSelect1">User type</label>
			      <select class="form-control" name="user_type" id="exampleSelect1">
			      	<option value="" > select </option>
			        <option value="owner"> Motorbike's Owner </option>
			        <option value="buyer">Buyer</option>
			      </select>
			      {!! $errors->first('user_type', '<span class="help-block text-danger"> :message </span>') !!}
			    </div>
			    
			    <button type="submit" class="btn btn-primary">Submit</button>
			  </fieldset>
			</form>
	</div>

</body>
</html>