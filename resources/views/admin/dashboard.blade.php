@if(empty(session()->get('auth_admin_email')))
	<script > window.location="/admin/login"</script>
@endif
<!DOCTYPE html>
<html>
<head>
	<title>MotoBlock Chain</title>
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.css') }}">
	<script type="text/javascript" src=" {{ url('assets/js/jquery-3.4.0.min.js') }} "></script>
	<script type="text/javascript" src=" {{ url('assets/js/bootstrap.js') }} "></script>
</head>
<body>
	@include('inc/header')
	<div class="container">
	<div class="row">
		<div class="col-sm-3">
		</div>
		<div class="col-sm-6">
			@if(session('info'))
					<div class="alert alert-danger col-sm-12">
						{{ session('info') }}
					</div>
				@endif
			<h1>Welecome to dashboard</h1>

		</div>
		<div class="col-sm-3">
		</div>

	</div>
		
	</div>

</body>
</html>