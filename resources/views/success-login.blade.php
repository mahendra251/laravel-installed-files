
@if(empty(session()->get('auth_email')))
	<script > window.location="/"</script>
@endif
<!DOCTYPE html>
<html>
<head>
	<title>MotoBlock Chain</title>
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.css') }}">
	<script type="text/javascript" src=" {{ url('assets/js/jquery-3.4.0.min.js') }} "></script>
	<script type="text/javascript" src=" {{ url('assets/js/bootstrap.js') }} "></script>
</head>
<body>
	@include('inc/header')
	<br><br><center><h1>{{ session()->get('auth_email') }} Successfully logged in </h1></center>

</body>
</html>