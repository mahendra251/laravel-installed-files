<!DOCTYPE html>
<html>
<head>
	<title>MotoBlock Chain</title>
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.css') }}">
	<script type="text/javascript" src=" {{ url('assets/js/jquery-3.4.0.min.js') }} "></script>
	<script type="text/javascript" src=" {{ url('assets/js/bootstrap.js') }} "></script>
</head>
<body>
	@include('inc/header')
	@if(session('info'))
		<div class="alert alert-success">
			{{ session('info') }}
		</div>
	@endif
	<br><br><center><h1>Welcome To MotoBlock chain</h1></center>

</body>
</html>