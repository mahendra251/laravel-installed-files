<!DOCTYPE html>
<html>
<head>
	<title>MotoBlock Chain</title>
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.css') }}">
	<script type="text/javascript" src=" {{ url('assets/js/jquery-3.4.0.min.js') }} "></script>
	<script type="text/javascript" src=" {{ url('assets/js/bootstrap.js') }} "></script>
</head>
<body>
	@include('inc/header')
	<br><br><center>
		@if(session('info'))
			<h1> {{ session('info') }}</h1>
			<p>Your successfully Registered .. Please check your mail and verify for the further process</p>
		@endif

		@if(session('invalid_token'))
			<h1> {{ session('invalid_token') }} </h1>
			<a href="{{url('/login') }}">Login</a>
			
		@endif

		
	</center>

</body>
</html>