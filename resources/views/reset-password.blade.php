<!DOCTYPE html>
<html>
<head>
	<title>MotoBlock Chain</title>
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.css') }}">
	<script type="text/javascript" src=" {{ url('assets/js/jquery-3.4.0.min.js') }} "></script>
	<script type="text/javascript" src=" {{ url('assets/js/bootstrap.js') }} "></script>
</head>
<body>
	@include('inc/header')
	<div class="container">
	<div class="row">
		<div class="col-sm-3">
		</div>
		<div class="col-sm-6">
			@if(session('info'))
					<div class="alert alert-danger col-sm-12">
						{{ session('info') }}
					</div>
				@endif

			@if(!empty($err_msg))
				<h4> {{ $err_msg }} </h4>
				@else
				<form action="{{ url('/generate_new_password') }}" method="post">
			{{ csrf_field() }}
			  <fieldset>
			    <legend>Generate   New Password</legend>
			    <input type="hidden" name="user_id" value="{{ $user_id }}"> 
			     <input type="hidden" name="token" value="{{ $token }}"> 
			    <div class="form-group ">
			    	
			      <label for="staticEmail" class=" col-form-label">New Password</label>
			      
			        <input type="password" name="new_password" class="form-control" id="staticEmail" Placeholder="Enter New Password" value="{{ old('new_password') }}">
			        {!! $errors->first('new_password', '<span class="help-block text-danger"> :message </span>') !!}
			   
			    </div>
			    
			    <div class="form-group">
			      <label for="exampleInputPassword1">Password</label>
			      <input type="password" name="confirm_password" class="form-control" id="exampleInputPassword1" placeholder="confirm  Password" value="{{ old('confirm_password') }}">
			      {!! $errors->first('confirm_password', '<span class="help-block text-danger"> :message </span>') !!}
			    </div>
			    
			    <button type="submit" class="btn btn-primary">Submit</button>
			  </fieldset>
			</form>

			@endif
				
			
		</div>
		<div class="col-sm-3">
		</div>

	</div>
		
	</div>

</body>
</html>