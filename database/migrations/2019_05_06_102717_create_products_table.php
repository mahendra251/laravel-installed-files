<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('brand_id');
            $table->integer('model_id');
            $table->string('frame_no');
            $table->string('frame_img');
            $table->string('purchase_date');
            $table->string('new_or_used');
            $table->string('previous_owner_no');
            $table->string('bike_imgs');
            $table->string('mileage');
            $table->string('mileage_img');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
