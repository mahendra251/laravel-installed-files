<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/testing', function () {  return view('dc_test');  });

Route::get('/', function () {  return view('home');  });
Route::get('/register', function () { return view('register'); });
Route::get('/login', function () { return view('login'); });
Route::get('/verify', function () { return view('verify'); });
Route::get('/registration-step-1', function () { return view('registration-step-1'); });
Route::get('/success-login', function () { return view('success-login'); });
Route::get('/forgot-password', function () { return view('forgot-password'); });

Route::get('/moter-cycle-registration', 'UserController@motor_cycle_registration');
Route::get('get-model-list','UserController@getModelList');


Route::post('/user_registration','UserController@insert_users');
Route::post('/user_login','UserController@login_user');
Route::get('/verify/{id}/{token}','UserController@verify_mail');
Route::post('/reg_step_1_insert','UserController@insert_reg_step1');
Route::post('/forgot_pass','UserController@forgot_password');
//Route::get('/reset-password/{id}/{token}','UserController@reset_password');
Route::get('/reset_password/{id}/{token}','UserController@reset_password');
Route::post('/generate_new_password','UserController@generate_new_password');
Route::post('/step_1_reg_insert','UserController@step_1_reg_insert');
Route::get('/logout','UserController@logout');

//Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');

//admin Routing
//Route::get('/moto_admin','admin\LoginController@index');

Route::get('/admin/login', function () { return view('admin/login'); });
Route::get('/admin/dashboard', function () { return view('admin/dashboard'); });
Route::post('/admin/admin_login','admin\LoginController@admin_login');

